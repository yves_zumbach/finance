from enum import Enum


class Currency(Enum):
    CHF = 1
    USD = 2
    EUR = 3
    BAT = 1000
    BTC = 1002
    COMP = 1009
    DAI = 1003
    EOS = 1004
    ETH = 1005
    OXT = 1006
    XLM = 1007
    XTZ = 1008
    MKR = 1009
    CGLD = 1010
    ALGO = 1011
    BAND = 1012
    FIL = 1013
    GRT = 1014
    NU = 1015
    DOGE = 1016
    MANA = 1017
    ICP = 1018
    FET = 1019
    AMP = 1020
    NEAR = 1021
    LUNA = 1022
    UST = 1023
    USDC = 1024

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


currencies_full_name = {
    Currency.CHF: 'Swiss Franc',
    Currency.USD: 'US Dollar',
    Currency.EUR: 'Euro',
    Currency.BAT: 'Basic Attention Token',
    Currency.BTC: 'Bitcoin',
    Currency.COMP: 'Compound',
    Currency.DAI: 'DAI',
    Currency.EOS: 'EOS',
    Currency.ETH: 'Ethereum',
    Currency.OXT: 'Orchid',
    Currency.XLM: 'Stellar Lumens',
    Currency.XTZ: 'Tezos',
    Currency.MKR: 'Maker',
    Currency.CGLD: 'Celo',
    Currency.ALGO: 'Algorand',
    Currency.BAND: 'Band',
    Currency.FIL: 'Filecoin',
    Currency.GRT: 'The Graph',
    Currency.NU: 'Nu Cypher',
    Currency.DOGE: 'DOGE Coin',
    Currency.MANA: 'Decentraland MANA',
    Currency.ICP: 'Internet Computer Token',
    Currency.FET: 'Fetch.ai',
    Currency.AMP: 'AMP',
    Currency.NEAR: 'NEAR',
    Currency.LUNA: 'LUNA',
    Currency.UST: 'TerraUSD',
    Currency.USDC: 'USDCoin',
}
