from collections import defaultdict
import logging
import platform


class TerminalColor:
    """
    Terminal character sequences that enable pretty printing.
    """
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

    GREY = '\033[90m'
    RED = '\033[91m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    BLUE = '\033[94m'
    PURPLE = '\033[95m'
    CYAN = '\033[96m'


class ColorFormatter(logging.Formatter):
    """
    Colors logs on platforms that support it.
    """
    IS_PLATFORM_COLOR_COMPATIBLE = platform.system() in ['Linux', 'Darwin']
    COLORS_FOR_LEVEL = defaultdict(lambda: '',
                                   DEBUG=TerminalColor.GREY,
                                   NEWLINE=TerminalColor.GREY,
                                   SECONDARY=TerminalColor.GREY,
                                   CMD_OUT='',
                                   INFO='',
                                   PHASE=TerminalColor.BOLD + TerminalColor.PURPLE,
                                   CMD=TerminalColor.BOLD,
                                   SUCCESS=TerminalColor.BOLD + TerminalColor.GREEN,
                                   EMPHASIS=TerminalColor.BOLD,
                                   QUESTION=TerminalColor.BLUE,
                                   WARNING=TerminalColor.BOLD + TerminalColor.YELLOW,
                                   ERROR=TerminalColor.RED,
                                   CRITICAL=TerminalColor.BOLD + TerminalColor.RED
                                   ) if IS_PLATFORM_COLOR_COMPATIBLE else defaultdict(lambda: '')
    COLOR_STOP = TerminalColor.END if IS_PLATFORM_COLOR_COMPATIBLE else ''
    PRINT_LEVEL_NAME = {'SUCCESS', 'WARNING', 'ERROR', 'CRITICAL'}

    def format(self, record):
        before = self.COLORS_FOR_LEVEL[record.levelname]
        level_name = (record.levelname + ': ') if record.levelname in self.PRINT_LEVEL_NAME else ''
        return f'{before}{level_name}%(message)s{self.COLOR_STOP}' % {
            'name': record,
            'message': record.msg,
            'time': self.formatTime(record, datefmt='%Y/%m/%d %H:%M:%S'),
        }


def setup_logger():
    """
    Setups the root logger.
    :return: Nothing
    """

    # Root Logger
    logger = logging.getLogger()

    # Add NEWLINE level to logger
    logging_newline = 13  # between DEBUG and INFO
    logging.addLevelName(logging_newline, 'NEWLINE')

    def newline(self, *args, **kwargs):
        if self.isEnabledFor(logging_newline):
            self._log(logging_newline, '', args, **kwargs)

    logging.Logger.newline = newline

    # Add SECONDARY level to logger
    logging_secondary = 15  # between DEBUG and INFO
    logging.addLevelName(logging_secondary, 'SECONDARY')

    def secondary(self, msg, *args, **kwargs):
        if self.isEnabledFor(logging_secondary):
            self._log(logging_secondary, msg, args, **kwargs)

    logging.Logger.secondary = secondary

    # Add COMMAND_OUTPUT level to logger
    logging_command_output = 17  # between DEBUG and INFO
    logging.addLevelName(logging_command_output, 'CMD_OUT')

    def command_output(self, msg, *args, **kwargs):
        if self.isEnabledFor(logging_command_output):
            self._log(logging_command_output, msg.rstrip('\n'), args, **kwargs)

    logging.Logger.command_output = command_output
    logging.Logger.cmd_out = command_output

    # Add COMMAND level to logger
    logging_command = 19  # between DEBUG and INFO
    logging.addLevelName(logging_command, 'CMD')

    def command(self, msg, *args, **kwargs):
        if self.isEnabledFor(logging_command):
            self._log(logging_command, msg, args, **kwargs)

    logging.Logger.command = command
    logging.Logger.cmd = command

    # Add PHASE level to logger
    logging_phase = 21  # between INFO and WARNING
    logging.addLevelName(logging_phase, 'PHASE')

    def phase(self, msg, *args, **kwargs):
        if self.isEnabledFor(logging_phase):
            self._log(logging_phase, msg, args, **kwargs)

    logging.Logger.phase = phase

    # Add SUCCESS level to logger
    logging_success = 25  # between INFO and WARNING
    logging.addLevelName(logging_success, 'SUCCESS')

    def success(self, msg, *args, **kwargs):
        if self.isEnabledFor(logging_success):
            self._log(logging_success, msg, args, **kwargs)

    logging.Logger.success = success

    # Add EMPHASIS level to logger
    logging_emphasis = 26  # between INFO and WARNING
    logging.addLevelName(logging_emphasis, 'EMPHASIS')

    def emphasis(self, msg, *args, **kwargs):
        if self.isEnabledFor(logging_emphasis):
            self._log(logging_emphasis, msg, args, **kwargs)

    logging.Logger.emphasis = emphasis
    logging.Logger.emph = emphasis

    # Add EMPHASIS level to logger
    logging_question = 27  # between INFO and WARNING
    logging.addLevelName(logging_question, 'QUESTION')

    def question(self, msg, *args, **kwargs):
        if self.isEnabledFor(logging_question):
            self._log(logging_question, msg, args, **kwargs)

    logging.Logger.question = question

    # Set logger level.
    logger.setLevel(logging_newline)

    # Print logs to the console
    console_handler = logging.StreamHandler()
    logger.addHandler(console_handler)

    # Pretty print if platform supports it
    console_handler.setFormatter(ColorFormatter())
