import json
import logging

import jsonpickle
from os.path import isfile
from typing import Optional

from finance.settings import Settings
from finance.utils import input_int, Singleton


logger = logging.getLogger(__name__)


class Portfolio:

    def __init__(self, name: str):
        self.name = name

    @classmethod
    def create_portfolio(cls) -> 'Portfolio':
        logger.phase('Creating a new Portfolio.')
        logger.newline()
        name = input('Portfolio name: ')
        logger.newline()

        return Portfolio(name=name)

    def __repr__(self):
        return f'Portfolio({self.name})'

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False


class Portfolios(metaclass=Singleton):

    PORTFOLIOS_FILE_NAME = 'portfolios.json'

    def __init__(self):
        self.portfolios: [Portfolio] = []
        self.portfolios_filepath = Settings().base_path / Portfolios.PORTFOLIOS_FILE_NAME
        self.read_portfolios_from_file()

    def read_portfolios_from_file(self) -> [Portfolio]:
        if not isfile(self.portfolios_filepath):
            with open(self.portfolios_filepath, 'w') as f:
                f.write('[]')
        with open(self.portfolios_filepath, 'r') as f:
            content = f.read()
        unparsed_portfolios = json.loads(content)
        self.portfolios = [jsonpickle.decode(unparsed_portfolio) for unparsed_portfolio in unparsed_portfolios]

    def write_portfolios_to_file(self):
        serialized_portfolios: [str] = [jsonpickle.encode(portfolio) for portfolio in self.portfolios]
        with open(self.portfolios_filepath, 'w') as f:
            f.write(json.dumps(serialized_portfolios))


def print_available_portfolios(print_none_portfolio=True):
    portfolios = Portfolios()
    logger.info('Available portfolios:')
    if print_none_portfolio:
        logger.info('- [-1] Do not specify')
    for index, portfolio in enumerate(portfolios.portfolios):
        logger.info(f'- [{index}] {portfolio}')


def input_portfolio(prompt: str, accept_none_portfolio=True) -> Optional[Portfolio]:
    portfolios = Portfolios()
    print_available_portfolios(accept_none_portfolio)
    while True:
        portfolio_index = input_int(prompt)
        if portfolio_index in range(len(portfolios.portfolios)):
            return portfolios.portfolios[portfolio_index]
        elif accept_none_portfolio and portfolio_index == -1:
            return None
        else:
            logger.error('Invalid index. Please enter a valid portfolio index.')

