from tabulate import tabulate

from finance.currencies import Currency
from finance.settings import Settings


class CurrencyPosition:
    """
    Tracks the amount of a given currency that someone has and the average
    price at which the currency was bought in the root currency.
    """

    def __init__(self,
                 avg_buying_price_in_root_currency: float,
                 amount: float,
                 currency: Currency,
                 invested_root_currency_amount: float = 0,
                 received_equivalent_in_root_currency: float = 0):
        """
        :param avg_buying_price_in_root_currency: The average price at which
        the token you have were bought/received expressed in the root currency.
        :param amount: The total amount of a token that you own.
        :param currency: The currency that this object describes.
        :param invested_root_currency_amount: The amount of root currency
        that you invested in this currency.
        :param received_equivalent_in_root_currency: The amount of this
        currency that you received.
        """
        self.avg_buying_price_in_root_currency = avg_buying_price_in_root_currency
        self.amount = amount
        self.currency = currency
        self.effectively_invested_root_currency_amount: float = invested_root_currency_amount
        self.received_amount_in_root_currency: float = received_equivalent_in_root_currency

    def sell(self, amount: float):

        # The percentage of the current position that was received instead of
        # bought.
        percentage_received = self.received_amount_in_root_currency / self.virtually_invested_in_currency()

        equivalent_amount_in_root_currency = amount * self.avg_buying_price_in_root_currency

        self.received_amount_in_root_currency -= percentage_received * equivalent_amount_in_root_currency
        self.received_amount_in_root_currency = max(0., self.received_amount_in_root_currency)

        self.effectively_invested_root_currency_amount -= (1 - percentage_received) * equivalent_amount_in_root_currency
        self.effectively_invested_root_currency_amount = max(0., self.effectively_invested_root_currency_amount)

        self.amount -= amount
        self.amount = max(0., self.amount)

        return amount * self.avg_buying_price_in_root_currency

    def buy(self, amount: float, equivalent_root_currency_amount: float):
        total_amount = self.amount + amount
        self.avg_buying_price_in_root_currency = (self.amount * self.avg_buying_price_in_root_currency +
                                                  equivalent_root_currency_amount) / total_amount

        self.effectively_invested_root_currency_amount += equivalent_root_currency_amount
        self.amount = total_amount

    def receive(self, amount: float, equivalent_root_currency_amount: float):
        total_amount = self.amount + amount
        self.avg_buying_price_in_root_currency = (self.amount * self.avg_buying_price_in_root_currency +
                                                  equivalent_root_currency_amount) / total_amount

        self.received_amount_in_root_currency += equivalent_root_currency_amount
        self.amount = total_amount

    def virtually_invested_in_currency(self) -> float:
        """
        :return: the total amount of root currency virtually invested (i.e. including received amounts) in this currency.
        """
        return self.effectively_invested_root_currency_amount + self.received_amount_in_root_currency

    def __repr__(self):
        return f'{round(self.amount, 2)} {self.currency} bought at ' \
               f'{round(self.avg_buying_price_in_root_currency, 2)} {Settings().root_currency}/{self.currency} ' \
               f'on average ' \
               f'(invested: {round(self.effectively_invested_root_currency_amount, 2)} {Settings().root_currency}, ' \
               f'received: {round(self.received_amount_in_root_currency, 2)} {Settings().root_currency})'


class CurrencyPositions:
    """
    Monitors how much of each currency one holds and what the average exchange rate to obtain this currency was.
    """

    def __init__(self):
        self.currency_positions = {}
        for currency in Currency:
            self.currency_positions[currency] = CurrencyPosition(0, 0, currency)
        self.currency_positions[Settings().root_currency].avg_buying_price_in_root_currency = 1

    def __getitem__(self, item):
        return self.currency_positions[item]

    def __setitem__(self, key, value):
        self.currency_positions[key] = value

    def __str__(self):
        rows = []
        headers = [
            'Currency',
            'Amount',
            f'Avg Buying Price {Settings().root_currency}/Currency',
            f'Invested {Settings().root_currency}',
            f'Received {Settings().root_currency}']
        for currency_position in self.currency_positions.values():
            rows.append([
                currency_position.currency,
                round(currency_position.amount, 3),
                round(currency_position.avg_buying_price_in_root_currency, 3),
                round(currency_position.effectively_invested_root_currency_amount, 3),
                round(currency_position.received_amount_in_root_currency, 3)
            ])
        return str(tabulate(rows, headers=headers))
