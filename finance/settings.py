import json

from finance.currencies import Currency
from finance.folders import dbs_folder
from finance.utils import Singleton, input_db


class Settings(metaclass=Singleton):
    """
    The settings of this program.
    They are imported from a configuration file named SETTINGS_FILE_NAME in
    the database folder.
    """

    SETTINGS_FILE_NAME = 'settings.json'

    def __init__(self):
        self.db = input_db()
        print(f'Using db {self.db}')
        self.base_path = dbs_folder / self.db

        with open(self.base_path / Settings.SETTINGS_FILE_NAME) as f:
            json_settings = json.loads(f.read())

        self.root_currency: Currency = Currency[str.upper(json_settings['root_currency'])]
        print(f'Using root currency {self.root_currency}')
