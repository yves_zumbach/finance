import logging
from typing import Dict

from tabulate import tabulate
from tqdm import tqdm

from finance.alpha_vantage import AlphaVantage
from finance.currencies import Currency, currencies_full_name
from finance.currency_position import CurrencyPositions, CurrencyPosition
from finance.events import Events, ExchangeCurrencies, \
    TransferBetweenPortfolios, ReceiveCurrency, UseCurrency, Event
from finance.utils import string_bold_green, string_bold_red, string_bold
from finance.settings import Settings

logger = logging.getLogger(__name__)


def compute_benefit(args):

    # Keep only the currency exchanges
    events = [event for event in Events().events
              if isinstance(event, ExchangeCurrencies)
              or isinstance(event, TransferBetweenPortfolios)
              or isinstance(event, ReceiveCurrency)
              or isinstance(event, UseCurrency)]
    events.sort(key=lambda t: t.timestamp)

    # Benefits realized by past transactions.
    past_benefits = 0

    # Table that contains the average effective exchange rate and the total amount possessed for all foreign currencies
    trading_positions = CurrencyPositions()

    # The total amount of fees paid.
    trading_fees = 0
    total_ever_invested_root_currency = 0

    # Table that contains the amounts of non-root currencies used for end
    # products or services, i.e. stuff that should be removed from the ledger
    # when estimating losses and benefits.
    currency_used_positions = CurrencyPositions()
    # The fees paid when using currencies, i.e. not when
    # trading.
    currency_used_fee_positions = CurrencyPositions()
    # A list containing all the stuffs that you bought, the equivalent
    # amount of root currency used to pay for the stuff and the equivalent
    # amount of root currency that you paid in fees.
    stuffs_bought_and_equivalent_price_paid_and_equivalent_fee: [(
        UseCurrency, float, float)] = []

    for event in events:
        if isinstance(event, TransferBetweenPortfolios):
            trading_positions[event.currency].amount -= event.fee
            if event.currency == Settings().root_currency:
                trading_fees += event.fee
            else:
                trading_fees += trading_positions[event.currency].avg_buying_price_in_root_currency * event.fee

        elif isinstance(event, ReceiveCurrency):
            if event.received_currency == Settings().root_currency:
                logger.error(f'Receiving root currency is not handled! Skipping event {event}.')
                continue

            if event.equivalent_to_currency != Settings().root_currency:
                logger.error(f'Not supporting non root currency equivalents for ReceiveCurrency events!'
                             f' Skipping event {event}.')
                continue

            trading_positions[event.received_currency].receive(event.received_amount, event.equivalent_to_amount)

        elif isinstance(event, ExchangeCurrencies):

            from_position: CurrencyPosition = trading_positions[event.from_currency]
            to_position: CurrencyPosition = trading_positions[event.to_currency]

            # Warning if impossible transaction
            if event.from_currency != Settings().root_currency \
                    and (from_position.amount - event.from_currency_amount) \
                    * from_position.avg_buying_price_in_root_currency < -2:
                logger.warning(f'Some transactions stored on file are impossible. At time {event.timestamp}, you had '
                               f'{trading_positions.currency_positions[event.from_currency].amount} '
                               f'{event.from_currency} '
                               f'and could therefore not complete the following transaction: {event}.'
                               f'I will assume that the transaction was executed nonetheless and set the '
                               f'{trading_positions.currency_positions[event.from_currency].amount} balance to 0.')

            trading_fees += event.fee * from_position.avg_buying_price_in_root_currency

            # Selling foreign currencies back to root currency.
            if event.to_currency == Settings().root_currency:
                cost_in_root_currency: float = from_position.sell(event.from_currency_amount)
                past_benefits += event.to_currency_amount - cost_in_root_currency

            # Buying foreign currencies
            elif event.from_currency == Settings().root_currency:
                total_ever_invested_root_currency += event.from_currency_amount
                to_position.buy(event.to_currency_amount, event.from_currency_amount)

            # Exchanging foreign currencies
            else:
                from_position.sell(event.from_currency_amount)
                to_position.buy(event.to_currency_amount,
                                event.from_currency_amount * from_position.avg_buying_price_in_root_currency)

        elif isinstance(event, UseCurrency):

            if event.currency == Settings().root_currency:
                logger.error(f"Using some root currency {Settings().root_currency} is invalid. {event} will be ignored.")
                continue

            # Remove the used currency and the fees from the benefits/losses accounting.
            # They will now be handled as being part of the currencies used
            # for sink transactions.
            trading_positions[event.currency].sell(event.amount)
            trading_positions[event.transaction_fee_currency].sell(event.transaction_fee_amount)

            # Track the used currencies
            equivalent_root_currency_for_buy = \
                event.amount * trading_positions[
                    event.currency].avg_buying_price_in_root_currency
            currency_used_positions[event.currency].buy(
                event.amount,
                equivalent_root_currency_for_buy
            )

            # Track the fees of UseCurrency.
            equivalent_root_currency_for_fee = \
                event.transaction_fee_amount * \
                trading_positions[
                    event.transaction_fee_currency].avg_buying_price_in_root_currency
            currency_used_fee_positions[event.transaction_fee_currency].buy(
                event.transaction_fee_amount,
                equivalent_root_currency_for_fee
            )

            stuffs_bought_and_equivalent_price_paid_and_equivalent_fee\
                .append((event, equivalent_root_currency_for_buy, equivalent_root_currency_for_fee))

        else:
            logger.error(f'Unrecognized event type: {event}. Skipping event.')

    logger.newline()

    print_transaction_history(
        events
    )
    print_currency_used(
        currency_used_positions,
        currency_used_fee_positions
    )
    print_what_currencies_were_used_to(
        stuffs_bought_and_equivalent_price_paid_and_equivalent_fee
    )
    print_portfolio_summary(
        trading_positions,
        trading_fees,
        total_ever_invested_root_currency,
        past_benefits
    )


def print_transaction_history(events: [Event]) -> None:
    """
    Prints a summary of all the transactions that you performed.
    :return: None
    """
    logger.phase('Transaction History')
    logger.newline()
    for event in events:
        logger.info(event)
    logger.newline()


def print_currency_used(
        currency_used_positions: CurrencyPositions,
        currency_used_fee_positions: CurrencyPositions
) -> None:
    """
    Prints what currencies were used.
    :param currency_used_positions: A `CurrencyPositions` object containing the currencies used.
    :param currency_used_fee_positions:  A `CurrencyPositions` object containing the currencies used in fees.
    :return: None
    """
    logger.phase('Currencies Used')
    logger.newline()
    equivalent_root_currency_of_used_currencies = 0
    headers = [
        'Amount',
        'Currency',
        'Equivalent To',
        'Avg Buy Price',
    ]
    rows = []
    for position in currency_used_positions.currency_positions.values():
        if position.amount == 0:
            continue
        equivalent_root_currency_of_used_currency = position.amount * position.avg_buying_price_in_root_currency
        rows.append([
            position.amount,
            position.currency,
            f'{round(equivalent_root_currency_of_used_currency, 2)} {Settings().root_currency}',
            f'{round(position.avg_buying_price_in_root_currency, 2)} {position.currency}/{Settings().root_currency}'
        ])
        equivalent_root_currency_of_used_currencies += equivalent_root_currency_of_used_currency
    total_equivalent_fees = 0
    for position in currency_used_fee_positions.currency_positions.values():
        total_equivalent_fees += position.amount * position.avg_buying_price_in_root_currency
    logger.info(tabulate(rows, headers=headers))
    logger.newline()
    logger.info(
        f'You have used an equivalent of {round(equivalent_root_currency_of_used_currencies, 2)} {Settings().root_currency}.')
    logger.info(
        f'You paid the equivalent of {round(total_equivalent_fees, 2)} {Settings().root_currency} in fees.')
    logger.newline()


def print_what_currencies_were_used_to(
        stuffs_bought_and_equivalent_price_paid_and_equivalent_fee: [(UseCurrency, float, float)]
) -> None:
    """
    Prints what currencies were used to do.
    :param stuffs_bought_and_equivalent_price_paid_and_equivalent_fee: An array containing the UseCurrency events, the
    equivalent root price of the transaction amount and the equivalent root currency of the fees.
    :return: None
    """

    logger.phase('Currencies Used to')
    logger.newline()
    if len(stuffs_bought_and_equivalent_price_paid_and_equivalent_fee) == 0:
        logger.info('No currencies used.')
    else:
        for event, price, fee in \
                stuffs_bought_and_equivalent_price_paid_and_equivalent_fee:
            logger.info(f'- {event.currency_used_to} ('
                        f'{event.amount} {event.currency} equivalent to {round(price, 2)} {Settings().root_currency}, '
                        f'fee: {event.transaction_fee_amount} {event.transaction_fee_currency} equivalent to '
                        f'{round(fee, 2)} {Settings().root_currency})')
    logger.newline()


def fetch_realtime_exchange_rate(currencies_to_fetch: [Currency]) -> (Dict[Currency, float], Dict[Currency, str]):
    """
    Fetches the realtime sell prices of the provided currencies.
    :param currencies_to_fetch: The list of currencies for which to fetch the realtime sell price.
    :return: A mapping from currency to the realtime sell price for the currencies that could be resolved,
    a mapping from currency to the reason why the currency could not be retrieved for the other currencies.
    """
    logger.info('Fetching Realtime Exchange Rates from Alpha Vantage')
    logger.newline()

    realtime_sell_price = {}
    unsupported_currencies: Dict[Currency, str] = {}

    for currency in tqdm(currencies_to_fetch):
        rt_sell_price = AlphaVantage().get_to_currency_per_from_currency_exchange_rate(
            from_currency=currency,
            to_currency=Settings().root_currency)
        if isinstance(rt_sell_price, float):
            realtime_sell_price[currency] = rt_sell_price
        else:
            reason = rt_sell_price
            unsupported_currencies[currency] = reason

    if len(unsupported_currencies) > 0:
        logger.newline()
        for currency, reason in unsupported_currencies.items():
            logger.warning(f'Could not retrieve exchange rate for {currency}. '
                           f'Limiting output for {currency}. '
                           f'Reason: {reason}')
    logger.newline()

    return realtime_sell_price, unsupported_currencies


def print_portfolio_summary(
        trading_positions: CurrencyPositions,
        trading_fees: float,
        total_ever_invested_root_currency: float,
        past_benefits: float
) -> None:
    """
    Print a summary of the losses and benefits of the portfolio.
    :param trading_positions: The current positions of the portfolio.
    :param trading_fees: The total trading fees incurred by the portfolio, expressed in root currency.
    :param total_ever_invested_root_currency: The total amount of root currency invested in this portfolio.
    :param past_benefits: The past benefits of the portfolio in root currency (can be negative).
    :return: None
    """
    logger.phase('Current Portfolio Summary')
    logger.newline()

    # Filter to keep only the trading position that are open.
    non_null_trading_positions = [
        currency_position for currency_position
        in trading_positions.currency_positions.values()
        if currency_position.amount != 0
        and currency_position.avg_buying_price_in_root_currency != 0
    ]

    realtime_sell_price, unsupported_currencies = fetch_realtime_exchange_rate(
        [position.currency for position in non_null_trading_positions]
    )

    non_null_supported_currency_positions = [position for position in non_null_trading_positions
                                             if position.currency not in unsupported_currencies.keys()]

    # The total amount of money that is currently invested in foreign currencies
    effectively_invested_root_currency = sum([
        position.effectively_invested_root_currency_amount
        for position
        in trading_positions.currency_positions.values()
        if position.currency != Settings().root_currency
    ])

    rows = []
    headers = ['Amount',
               'Avg Buy Price',
               f'Invested {Settings().root_currency}',
               'Received',
               'RT Sell Price',
               'Δ Value',
               '% Investments',
               '% Portfolio']

    sell_all_positions_to_root_currency = 0
    for position in non_null_supported_currency_positions:
        sell_all_positions_to_root_currency += \
            position.amount * realtime_sell_price[position.currency]

    for position in non_null_trading_positions:

        root_currency_per_currency_string = ' ' + str(Settings().root_currency) + '/' + str(position.currency)

        percentage_of_portfolio_investment = str(
            round(position.effectively_invested_root_currency_amount
                  / effectively_invested_root_currency * 100, 2)) + '%' \
            if effectively_invested_root_currency != 0 \
            else 'NA'

        # Unsupported currencies
        if position.currency in unsupported_currencies:
            rows.append([
                f'{round(position.amount, 2)} {position.currency}',
                f'{round(position.avg_buying_price_in_root_currency, 2)}{root_currency_per_currency_string}',
                f'{round(position.effectively_invested_root_currency_amount, 2)} {Settings().root_currency}',
                f'{round(position.received_amount_in_root_currency, 2)} {Settings().root_currency}',
                'NA',
                'NA',
                percentage_of_portfolio_investment,
                'NA',
            ])
            continue

        if position.amount * position.avg_buying_price_in_root_currency < 1:
            continue

        sell_price = realtime_sell_price[position.currency]

        value_increase_percentage_compared_to_average_buying_price = \
            (sell_price / position.avg_buying_price_in_root_currency - 1) * 100
        currency_went_up = sell_price > position.avg_buying_price_in_root_currency
        string_amount_increase_percentage_compared_to_average_buying_price = \
            str(round(value_increase_percentage_compared_to_average_buying_price, 2)) + '%'

        sell_currency_to_root_currency = position.amount * sell_price

        benefits_if_sell_all_now = str(round(
            sell_currency_to_root_currency - position.effectively_invested_root_currency_amount,
            2
        )) + ' ' + str(Settings().root_currency)

        percentage_of_current_portfolio = sell_currency_to_root_currency / sell_all_positions_to_root_currency * 100

        rows.append([
            str(round(position.amount, 2)) + ' ' + str(position.currency),
            str(round(position.avg_buying_price_in_root_currency, 2)) + root_currency_per_currency_string,
            str(round(position.effectively_invested_root_currency_amount, 2)) + ' ' + str(Settings().root_currency),
            str(round(position.received_amount_in_root_currency, 2)) + ' ' + str(Settings().root_currency),
            str(round(sell_price, 2)) + root_currency_per_currency_string + ' -> '
            + str(round(sell_price * position.amount, 2)) + ' ' + str(Settings().root_currency),
            string_bold_green(
                benefits_if_sell_all_now + ' (' + string_amount_increase_percentage_compared_to_average_buying_price + ')'
            ) if currency_went_up else string_bold_red(
                benefits_if_sell_all_now + ' (' + string_amount_increase_percentage_compared_to_average_buying_price + ')'
            ),
            percentage_of_portfolio_investment,
            str(round(percentage_of_current_portfolio, 2)) + '%',
        ])
    logger.info(tabulate(rows, headers=headers))
    logger.newline()
    logger.info(f'Total fees paid: {round(trading_fees, 2)} {Settings().root_currency}, represents '
                f'{round(trading_fees / total_ever_invested_root_currency * 100, 2)}% of your total investments.')
    logger.newline()

    is_benefits = sell_all_positions_to_root_currency > effectively_invested_root_currency
    benefits_percentage = (sell_all_positions_to_root_currency
                           / effectively_invested_root_currency - 1) * 100
    benefits_absolute_value = sell_all_positions_to_root_currency - effectively_invested_root_currency
    sell_all_to_root_currency_string = f'{round(sell_all_positions_to_root_currency, 2)} {Settings().root_currency}'
    benefits_string = f'{round(abs(benefits_absolute_value), 2)} {Settings().root_currency} ' \
                      + f'({round(benefits_percentage, 1)}%)'

    past_benefits_string = f'{round(abs(past_benefits), 2)} {Settings().root_currency}'

    logger.info(tabulate([
        [f'Past {"Benefits" if past_benefits >= 0 else "Losses"}',
         (string_bold_green(past_benefits_string) if past_benefits >= 0 else string_bold_red(past_benefits_string))],
        ['Current Investments',
         string_bold(
             f'{round(effectively_invested_root_currency, 2)} {Settings().root_currency}')],
        ['Sell Current Portfolio Now', (string_bold_green(sell_all_to_root_currency_string) if is_benefits
                                        else string_bold_red(sell_all_to_root_currency_string))],
        [f'{"Benefits" if is_benefits else "Losses"}',
         (string_bold_green(benefits_string) if is_benefits else string_bold_red(benefits_string))]
    ]))
    logger.newline()