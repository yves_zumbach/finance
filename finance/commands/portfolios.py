import logging

from finance.events import Events
from finance.portfolios import Portfolios, Portfolio, print_available_portfolios
from finance.utils import abort, input_int

logger = logging.getLogger(__name__)


def create_portfolio():
    portfolios: Portfolios = Portfolios()
    logger.newline()

    new_portfolio = Portfolio.create_portfolio()
    portfolios.portfolios.append(new_portfolio)
    portfolios.write_portfolios_to_file()

    logger.success(f'Created {new_portfolio}')


def delete_portfolio():
    portfolios: Portfolios = Portfolios()
    logger.newline()
    logger.info('Deleting an existing portfolio.')
    logger.warning('This will also delete all events that used this portfolio.')
    logger.newline()
    print_available_portfolios(print_none_portfolio=False)
    logger.newline()
    index_of_portfolio_to_delete = input_int('Index of portfolio to delete: ')
    logger.newline()
    if index_of_portfolio_to_delete not in range(len(portfolios.portfolios)):
        abort('Invalid index entered.')
    else:
        # Compute all events that would be deleted
        events: Events = Events()
        events_to_keep = []
        portfolio_to_delete = portfolios.portfolios[index_of_portfolio_to_delete]
        events_to_delete = []
        for event in events.events:
            if event.from_portfolio == portfolio_to_delete or event.to_portfolio == portfolio_to_delete:
                logger.warning(f'This will delete event {event}.')
                events_to_delete.append(event)
            else:
                events_to_keep.append(event)

        # Make sure the user knows what it's doing
        if len(events_to_delete) > 0:
            logger.newline()
            continue_input = input('Are you sure you want to continue (y/n)? ')
            logger.newline()
            if continue_input != 'y':
                logger.info('Nothing was deleted.')
                logger.success('Aborting now.')
                exit()

        # Delete events related to deleted portfolio
        events.events = events_to_keep
        events.write_events_to_file()

        for event in events_to_delete:
            logger.success(f'Deleted {event}')
        logger.newline()

        # Delete portfolio to delete
        portfolio_name = portfolios.portfolios[index_of_portfolio_to_delete]
        del portfolios.portfolios[index_of_portfolio_to_delete]
        portfolios.write_portfolios_to_file()
        logger.success(f'Successfully deleted portfolio {portfolio_name}.')


def list_portfolios():
    portfolios = Portfolios()
    logger.newline()
    if len(portfolios.portfolios) == 0:
        logger.info('No portfolios.')
    else:
        print_available_portfolios(print_none_portfolio=False)


def manage_portfolios(args):
    if args.action == 'create':
        create_portfolio()
    elif args.action == 'delete':
        delete_portfolio()
    elif args.action == 'list':
        list_portfolios()
