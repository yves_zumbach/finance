import logging

from finance.events import Event, event_types, Events
from finance.utils import abort, input_int

logger = logging.getLogger(__name__)


def create_event():
    events: Events = Events()
    logger.newline()
    logger.phase('Creating a new event.')
    logger.newline()
    logger.info('Possible types of events:')
    for key, data_type in event_types.items():
        logger.info(f'- [{key}] {data_type.__name__}')
    logger.newline()
    user_input = input('Type of event to create: ')
    logger.newline()

    if user_input in event_types:
        event_type: Event = event_types[user_input]
        event = event_type.create_event()
        logger.newline()

        events.events.append(event)
        events.write_events_to_file()

        logger.success('Added following event to the database:')
        logger.newline()
        logger.info(event)
    else:
        logger.error('Invalid event type. Please choose a valid value.')


def delete_event():
    events: Events = Events()
    logger.newline()
    logger.phase('Deleting an existing event.')
    logger.newline()
    for index, event in enumerate(events.events):
        logger.info(f'- [{index}] {event}')
    logger.newline()
    index_of_event_to_delete = input_int('Index of event to delete: ')
    logger.newline()

    if index_of_event_to_delete not in range(len(events.events)):
        abort('Invalid index entered.')
    else:
        del events.events[index_of_event_to_delete]
        events.write_events_to_file()
        logger.success('Successfully deleted event.')


def list_events():
    events = Events()
    logger.newline()
    if len(events.events) == 0:
        logger.info('No events.')
    else:
        events.events.sort(key=lambda e: e.timestamp)
        logger.info('Listing all events:')
        logger.newline()
        for event in events.events:
            logger.info(f'- {event}')


def manage_events(args):
    if args.action == 'create':
        create_event()
    elif args.action == 'delete':
        delete_event()
    elif args.action == 'list':
        list_events()
