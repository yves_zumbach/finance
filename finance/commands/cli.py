import argparse

from finance.commands.events import manage_events
from finance.commands.analysis import compute_benefit
from finance.commands.portfolios import manage_portfolios
from finance.logger import setup_logger
from finance.settings import Settings

def main():

    parser = argparse.ArgumentParser(description="Provide analysis functionalities for cryptocurrencies.")
    subparsers = parser.add_subparsers(dest='action')
    subparsers.required = True

    # Events
    parser_events = subparsers.add_parser('events',
                                          help='Manage events.',
                                          description='Manages events.')
    parser_events.add_argument('action',
                               choices=('create', 'delete', 'list'),
                               help='The action to perform on the events.')
    parser_events.set_defaults(func=manage_events)

    # Portfolios
    parser_portfolios = subparsers.add_parser('portfolios',
                                              help='Manage portfolios.',
                                              description='Manage portfolios.')
    parser_portfolios.add_argument('action',
                                   choices=('create', 'delete', 'list'),
                                   help='The action to perform on the portfolios.')
    parser_portfolios.set_defaults(func=manage_portfolios)

    # Analysis
    parser_analysis = subparsers.add_parser('analysis',
                                            help='Analyse your transaction history.',
                                            description='Analyse your transaction history.')
    parser_analysis.set_defaults(func=compute_benefit)

    setup_logger()
    Settings()
    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
