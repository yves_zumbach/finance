import datetime
import logging
from typing import Set, Optional

from finance.currencies import Currency, currencies_full_name
from finance.folders import dbs_folder

logger = logging.getLogger(__name__)


# Singleton Decorator
#############################


class Singleton(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


# Safe Input
#############################


def input_float(prompt) -> float:
    while True:
        flt = input(prompt)
        if flt == '':
            return 0
        try:
            return float(flt)
        except ValueError:
            logger.error('Not a valid value. Please enter a floating point number.')


def input_int(prompt) -> int:
    while True:
        integer = input(prompt)
        if integer == '':
            return 0
        try:
            return int(integer)
        except ValueError:
            logger.error('Not a valid value. Please enter an integer.')


def input_timestamp(prompt) -> datetime.datetime:
    now = datetime.datetime.now()
    while True:
        logger.info(f'Timestamp examples: 2020-03-01 13:45. Type enter for {now}')
        timestamp = input(prompt)
        if timestamp == '':
            return now
        try:
            timestamp = datetime.datetime.fromisoformat(timestamp)
            return timestamp
        except ValueError:
            logger.error('Not a valid timestamp. Please enter a valid timestamp.')


def parse_currency(currency_string: str) -> Currency:
    return Currency[currency_string]


def input_currency(prompt: str) -> Currency:
    print_available_currencies()
    while True:
        try:
            return parse_currency(str.upper(input(prompt)))
        except KeyError:
            logger.error('Could not parse currency.')
            continue


class ParsingError(BaseException):
    def __init__(self, reason):
        super(ParsingError, self).__init__()
        self.reason = reason


def input_amount(prompt: str) -> (float, Currency):
    while True:
        split_input = input(prompt).split(' ')
        try:
            if len(split_input) != 2:
                raise ParsingError('Provided string does not conform to having two elements in it.')
            try:
                amount = float(split_input[0])
            except ValueError as e:
                raise ParsingError(f'Could not parse the provided value: {e}')
            currency = parse_currency(split_input[1])
            if not currency:
                raise ParsingError(f'Could not parse currency. '
                                   f'List of valid codes: {[currency for currency in Currency]}')
            return amount, currency
        except ParsingError as e:
            logger.error(f'You must enter a float, then a valid currency 3 letter code (ISO currency code). '
                         f'Error reason: {e.reason}')


def input_range() -> Set[int]:
    output_range = set()
    range_unparsed = input('E.g. 0, 1, 4-5: ')
    for literal in range_unparsed.strip(' ').split(','):
        if '-' in literal:
            literal = literal.split('-')
            if len(literal) != 2:
                abort('Invalid range provided: ' + '-'.join(literal))
            for index in range(int(literal[0]), int(literal[1]) + 1):
                output_range.add(index)
        else:
            output_range.add(int(literal))
    return output_range


def input_db() -> str:
    dbs = []
    for db in dbs_folder.iterdir():
        if db.is_dir():
            dbs.append(db)
    print('Select a database to use among the following:')
    for index, db in enumerate(dbs):
        print(f'- [{index}] {db.name}')

    while True:
        db_index = input_int('Database index: ')
        if db_index in range(len(dbs)):
            return dbs[db_index]
        else:
            logger.error('Invalid index. Please enter a valid database index.')


# Pretty Printing
#############################

class TerminalColor:
    """
    Terminal character sequences that enable pretty printing.
    """
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def print_bold(text: str):
    print(TerminalColor.BOLD + text + TerminalColor.END)


def print_red(text: str):
    print(TerminalColor.RED + text + TerminalColor.END)


def print_bold_red(text: str):
    print(TerminalColor.BOLD + TerminalColor.RED + text + TerminalColor.END)


def print_bold_yellow(text: str):
    print(TerminalColor.BOLD + TerminalColor.YELLOW + text + TerminalColor.END)


def print_green(text: str):
    print(TerminalColor.GREEN + text + TerminalColor.END)


def print_bold_green(text: str):
    print(TerminalColor.BOLD + TerminalColor.GREEN + text + TerminalColor.END)


def string_bold(text: str) -> str:
    return TerminalColor.BOLD + text + TerminalColor.END


def string_bold_red(text: str) -> str:
    return TerminalColor.BOLD + TerminalColor.RED + text + TerminalColor.END


def string_bold_green(text: str) -> str:
    return TerminalColor.BOLD + TerminalColor.GREEN + text + TerminalColor.END


def print_available_currencies():
    """
    Prints all the available currencies.
    :return: nothing
    """
    logger.info('Available currencies:')
    for currency in Currency:
        logger.info(f'- {currency}: {currencies_full_name[currency]}')


# Abort
#############################

def abort(error_message: str, exception: Optional[BaseException] = None):
    """
    Aborts the current program and prints an error message if some was provided.
    :param error_message: The error message
    :param exception: The optional error to print.
    :return: Nothing
    """
    logger.critical(error_message)
    if exception is not None:
        logger.critical(exception)
    logger.critical('Exception occurred. Aborting now.')
    exit(1)
