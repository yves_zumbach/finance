from datetime import datetime
from time import sleep

import requests

from finance.currencies import Currency
from finance.utils import Singleton


class AlphaVantage(metaclass=Singleton):
    """
    Realtime exchange rates API.
    """

    time_interval_seconds = 61
    max_requests_per_time_interval = 5

    API_KEY = 'QFCXYAIPT0B550NQ'
    BASE_URL = 'https://www.alphavantage.co/query?'

    requests_since_last_timestamp = -1
    last_timestamp: datetime = None

    @classmethod
    def get_to_currency_per_from_currency_exchange_rate(cls, from_currency: Currency, to_currency: Currency):
        # throttling requests to max 5 requests / min
        if cls.requests_since_last_timestamp == -1:
            cls.last_timestamp = datetime.now()
            cls.requests_since_last_timestamp = 0

        elif cls.requests_since_last_timestamp >= cls.max_requests_per_time_interval:
            now = datetime.now()
            seconds_since_last_timestamp: int = (now - cls.last_timestamp).seconds
            if seconds_since_last_timestamp < cls.time_interval_seconds:
                sleep(cls.time_interval_seconds - seconds_since_last_timestamp)
                cls.last_timestamp = now
                cls.requests_since_last_timestamp = 0

        url = cls.BASE_URL + f'function=CURRENCY_EXCHANGE_RATE' + f'&from_currency={from_currency}' + \
              f'&to_currency={to_currency}' + f'&apikey={cls.API_KEY}'
        json = requests.get(url).json()

        cls.requests_since_last_timestamp += 1

        try:
            return float(json['Realtime Currency Exchange Rate']['5. Exchange Rate'])
        except KeyError:
            return json
