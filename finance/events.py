import datetime
import json
import logging

import jsonpickle
from os.path import isfile

from finance.currencies import Currency
from finance.portfolios import Portfolio, input_portfolio
from finance.settings import Settings
from finance.utils import input_float, input_timestamp, input_currency, Singleton, input_amount

logger = logging.getLogger(__name__)


class Event:

    def __init__(self,
                 timestamp: datetime.datetime,
                 remark: str):
        self.timestamp = timestamp
        self.remark = remark

    @classmethod
    def create_event(cls) -> 'Event':
        raise NotImplementedError(f'Method create_event not implemented for class {cls.__name__}')


class ExchangeCurrencies(Event):
    """
    One currency exchanged for another.
    """

    def __init__(self,
                 from_currency: Currency,
                 to_currency: Currency,
                 from_currency_amount: float,
                 to_currency_per_from_currency_exchange_rate: float,
                 fee: float,
                 from_portfolio: Portfolio,
                 to_portfolio: Portfolio,
                 exchange_platform: str,
                 timestamp: datetime.datetime,
                 remark: str):
        super(ExchangeCurrencies, self).__init__(
            timestamp=timestamp,
            remark=remark)
        self.from_currency = from_currency
        self.to_currency = to_currency
        self.from_currency_amount = from_currency_amount
        self.to_currency_per_from_currency_exchange_rate = to_currency_per_from_currency_exchange_rate
        self.fee = fee
        self.from_portfolio = from_portfolio
        self.to_portfolio = to_portfolio
        self.platform = exchange_platform

        self.to_currency_amount = (self.from_currency_amount - self.fee) * \
                                  self.to_currency_per_from_currency_exchange_rate

    @classmethod
    def create_event(cls) -> 'ExchangeCurrencies':
        logger.phase('Creating a new ExchangeCurrencies event.')
        logger.newline()

        from_currency = input_currency('From currency: ')
        logger.newline()
        to_currency = input_currency('To currency: ')
        logger.newline()
        from_currency_amount = input_float(f'Paid {from_currency}: ')
        logger.newline()
        received_currency = input_float(f'Received {to_currency}: ')
        logger.newline()
        fee = input_float(f'Fee (in {from_currency}): ')
        logger.newline()

        exchange_rate = received_currency / (from_currency_amount - fee)
        logger.info(f'Computed exchange rate: {exchange_rate} {to_currency}/{from_currency} '
                    f'= {1.0 / exchange_rate} {from_currency}/{to_currency}')
        logger.newline()

        from_portfolio = input_portfolio('Currency coming from portfolio: ')
        logger.newline()
        to_portfolio = input_portfolio('Currency loaded into portfolio: ')
        logger.newline()
        platform = input('Platform used to perform the exchange: ')
        logger.newline()
        timestamp = input_timestamp('Transaction timestamp: ')
        logger.newline()
        remark = input('Remarks if any: ')
        logger.newline()

        return ExchangeCurrencies(from_currency=from_currency,
                                  to_currency=to_currency,
                                  from_currency_amount=from_currency_amount,
                                  to_currency_per_from_currency_exchange_rate=exchange_rate,
                                  fee=fee,
                                  from_portfolio=from_portfolio,
                                  to_portfolio=to_portfolio,
                                  exchange_platform=platform,
                                  timestamp=timestamp,
                                  remark=remark)

    def __repr__(self):
        return f'ExchangeCurrencies({self.from_currency_amount} {self.from_currency} -> ' \
               + f'{self.to_currency_amount} {self.to_currency}, ' \
               + f'at {self.to_currency_per_from_currency_exchange_rate} ' \
               + f'{self.to_currency}/{self.from_currency}, ' \
               + f'with {self.fee} {self.from_currency} fee, ' \
               + f'from {self.from_portfolio if self.from_portfolio else "Unspecified"} ' \
               + f'to {self.to_portfolio if self.to_portfolio else "Unspecified"}, ' \
               + f'on {self.timestamp})'


class ReceiveCurrency(Event):
    """
    The world is sometimes nice, and you just receive currencies...
    """

    def __init__(self,
                 received_currency: Currency,
                 received_amount: float,
                 equivalent_to_currency: Currency,
                 equivalent_to_amount: float,
                 received_from: str,
                 to_portfolio: Portfolio,
                 timestamp: datetime.datetime,
                 remark: str):

        super(ReceiveCurrency, self).__init__(
            timestamp=timestamp,
            remark=remark)

        self.received_currency: Currency = received_currency
        self.received_amount: float = received_amount
        self.equivalent_to_currency: Currency = equivalent_to_currency
        self.equivalent_to_amount: float = equivalent_to_amount
        self.received_from: str = received_from
        self.to_portfolio = to_portfolio

    @classmethod
    def create_event(cls) -> 'ReceiveCurrency':
        logger.phase('Creating a new ReceiveCurrency event.')
        logger.newline()

        received_amount, received_currency = input_amount('Received amount (amount currency): ')
        logger.newline()
        equivalent_to_amount_at_buy_time = input_float(f'Equivalent to (??? {Settings().root_currency}): ')
        logger.newline()
        received_from = input('Currency received from: ')
        logger.newline()
        to_portfolio = input_portfolio('Currency loaded into portfolio: ')
        logger.newline()
        timestamp = input_timestamp('Transaction timestamp: ')
        logger.newline()
        remark = input('Remarks if any: ')
        logger.newline()

        return ReceiveCurrency(received_currency=received_currency,
                               received_amount=received_amount,
                               equivalent_to_currency=Settings().root_currency,
                               equivalent_to_amount=equivalent_to_amount_at_buy_time,
                               received_from=received_from,
                               to_portfolio=to_portfolio,
                               timestamp=timestamp,
                               remark=remark)

    def __repr__(self):
        return f'ReceiveCurrency({round(self.received_amount, 3)} {self.received_currency} ' \
               f'(equivalent to {self.equivalent_to_amount} ' \
               f'{self.equivalent_to_currency})' \
               f' from {self.received_from} into {self.to_portfolio}, ' \
               f'on {self.timestamp})'


class TransferBetweenPortfolios(Event):
    """
    Transfer some currencies from one portfolio to another.
    """

    def __init__(self,
                 currency: Currency,
                 currency_amount: float,
                 fee: float,
                 from_portfolio: Portfolio,
                 to_portfolio: Portfolio,
                 timestamp: datetime.datetime,
                 remark: str):
        """
        A transfer between portfolios.
        :param currency: The currency transferred.
        :param currency_amount: The total amount sent from the originating portfolio.
        :param fee: The fees paid to execute the transfer.
        :param from_portfolio: The portfolio the funds were coming from.
        :param to_portfolio: The portfolio the funds were loaded into.
        :param timestamp: The timestamp at which the transaction was conducted.
        :param remark: Some textual remark.
        """
        super(TransferBetweenPortfolios, self).__init__(
            timestamp=timestamp,
            remark=remark)
        self.currency = currency
        self.currency_amount = currency_amount
        self.fee = fee
        self.from_portfolio = from_portfolio
        self.to_portfolio = to_portfolio

    @classmethod
    def create_event(cls) -> 'TransferBetweenPortfolios':
        logger.phase('Creating a new TransferBetweenPortfolios event.')
        logger.newline()

        currency = input_currency('Currency transferred: ')
        logger.newline()
        currency_amount = input_float('Total amount sent: ')
        logger.newline()
        from_portfolio = input_portfolio('Source portfolio: ')
        logger.newline()
        to_portfolio = input_portfolio('Destination portfolio: ')
        logger.newline()
        fee = input_float(f'Fee (in {currency}): ')
        logger.newline()
        timestamp = input_timestamp('Transfer timestamp: ')
        logger.newline()
        remark = input('Remarks if any: ')
        logger.newline()

        return TransferBetweenPortfolios(currency=currency,
                                         currency_amount=currency_amount,
                                         from_portfolio=from_portfolio,
                                         fee=fee,
                                         to_portfolio=to_portfolio,
                                         timestamp=timestamp,
                                         remark=remark)

    def __repr__(self):
        return f'TransferBetweenPortfolios({round(self.currency_amount, 3)} {self.currency}, ' \
               + f'{self.from_portfolio} -> {self.to_portfolio}, ' + \
               f'{round(self.fee, 3)} {self.currency} fee, on {self.timestamp})'


class UseCurrency(Event):
    """
    Use some currencies to pay stuffs, like a service, some assets or a friend.
    """

    def __init__(self,
                 currency: Currency,
                 amount: float,
                 transaction_fee_currency: Currency,
                 transaction_fee_amount: float,
                 currency_used_to: str,
                 from_portfolio: Portfolio,
                 timestamp: datetime.datetime,
                 remark: str):
        """
        A transaction that you submitted to use some of your currencies.

        :param currency: The currency used.
        :param amount: The total amount sent from the originating portfolio.
        :param transaction_fee_currency: The currency of the transaction fees.
        :param transaction_fee_amount: The amount of fees paid for the
        transaction.
        :param currency_used_to: What the currency was used to do.
        Example: "Buy an ENS domain name" or "Pay some bucks to Alice"..
        :param from_portfolio: The portfolio from which the funds were taken.
        :param timestamp: The timestamp at which the transaction was conducted.
        :param remark: Some textual remark.
        """
        super(UseCurrency, self).__init__(
            timestamp=timestamp,
            remark=remark)
        self.currency = currency
        self.amount = amount
        self.transaction_fee_currency = transaction_fee_currency
        self.transaction_fee_amount = transaction_fee_amount
        self.currency_used_to = currency_used_to
        self.from_portfolio = from_portfolio

    @classmethod
    def create_event(cls) -> 'UseCurrency':
        logger.phase('Creating a new UseCurrency event.')
        logger.newline()

        amount, currency = \
            input_amount('Amount sent in transaction (amount currency): ')
        logger.newline()
        transaction_fee_amount, transaction_fee_currency = \
            input_amount('Transaction fee paid (amount currency): ')
        logger.newline()
        stuff_used_for = input("Currency used to: ")
        logger.newline()
        from_portfolio = input_portfolio('Portfolio used to send the transaction: ')
        logger.newline()
        timestamp = input_timestamp('Transfer timestamp: ')
        logger.newline()
        remark = input('Remarks if any: ')
        logger.newline()

        return UseCurrency(currency=currency,
                           amount=amount,
                           transaction_fee_currency=transaction_fee_currency,
                           transaction_fee_amount=transaction_fee_amount,
                           currency_used_to= stuff_used_for,
                           from_portfolio=from_portfolio,
                           timestamp=timestamp,
                           remark=remark)

    def __repr__(self):
        return f'UseCurrency(' \
               f'Paid {round(self.amount, 3)} {self.currency} (' \
               f'{self.transaction_fee_amount} ' \
               f'{self.transaction_fee_currency} fee) to {self.currency_used_to}, ' \
               f'on {self.timestamp})'


event_types = {
    'e': ExchangeCurrencies,
    'r': ReceiveCurrency,
    't': TransferBetweenPortfolios,
    'u': UseCurrency,
}


class Events(metaclass=Singleton):
    EVENTS_FILE_NAME = 'events.json'

    def __init__(self):
        self.events: [Event] = []
        self.events_filepath = Settings().base_path / Events.EVENTS_FILE_NAME
        self.read_events_from_file()

    def read_events_from_file(self) -> [Event]:
        if not isfile(self.events_filepath):
            with open(self.events_filepath, 'w') as f:
                f.write('[]')
        with open(self.events_filepath, 'r') as f:
            content = f.read()
        unparsed_events = json.loads(content)
        self.events = [jsonpickle.decode(unparsed_event) for unparsed_event in unparsed_events]

    def write_events_to_file(self):
        serialized_events: [str] = [jsonpickle.encode(event) for event in self.events]
        with open(self.events_filepath, 'w') as f:
            f.write(json.dumps(serialized_events))
