from cli.events import Events, Receive, ReceiveCurrency
from cli.settings import Settings
from cli.utils import input_float


converted_events = []


for event in Events().events:
    if isinstance(event, Receive):
        print(event, event.currency)
        equivalent_to_amount_at_buy_time = input_float(f'Equivalent to (??? {Settings().root_currency}): ')
        new_event = ReceiveCurrency(
            received_currency=event.currency,
            received_amount=event.amount,
            equivalent_to_currency=Settings().root_currency,
            equivalent_to_amount=equivalent_to_amount_at_buy_time,
            received_from=event.received_from,
            to_portfolio=event.to_portfolio,
            timestamp=event.timestamp,
            remark=event.remark
        )
        print(new_event)
        print()
        converted_events.append(new_event)
    else:
        converted_events.append(event)

print(converted_events)
stop = input('Continue? (Ctrl+C to stop now)')

Events().events = converted_events
Events().write_events_to_file()
