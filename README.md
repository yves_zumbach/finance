# Finance

A program to keep a clear overview of financial assets, especially cryptocurrencies.

## Installation

```shell script
pip install -r requirements.txt
```

Create a `resources/` folder inside of `src/main/`.
Add a file named `settings.json` containing the following:

```json
{
  "root_currency": "[ISO_CURRENCY_CODE]"
}
```

Where you replaced `[ISO_CURRENCY_CODE]` with the currency ISO code of your root money (like `USD` for example).

## Running

```shell script
./src/main/python/finance.py -h
```

It is recommended to create an alias to the `finance` program.
To accomplish this using `bash`, add the following line to `~/.bashrc`:

```shell script
alias finance="<path_to_repo>/src/main/python/finance.py"
```

## Documentation

Events are stored in the `src/main/resources/events.json` file.
Settings for the program are specified inside the `src/main/resources/settings.json` file.

It is a very good idea to commit the `src/resources/` folder to a version control system.
This folder contains all user-specific data.
